#include "equalityOperators.h"
#import <lwesamples.h>

bool operator==(const MKLweSample &left, const MKLweSample &right) {
	if (left.n != right.n || left.parties != right.parties || left.b != right.b) {
		return false;
	}
	for (unsigned int i = 0; i < left.n * left.parties; i++) {
		if (left.a[i] != right.a[i]) {
			return false;
		}
	}
	return true;
}

bool operator==(const std::shared_ptr<MKLweSample> &left, const std::shared_ptr<MKLweSample> &right) {
	return *left == *right;
}

bool operator!=(const std::shared_ptr<MKLweSample> &left, const std::shared_ptr<MKLweSample> &right) {
	return !(left == right);
}

bool equals(const LweSample &left, const LweSample &right, unsigned int n) {
	for (unsigned int i = 0; i < n; i++) {
		if (left.a[i] != right.a[i]) {
			return false;
		}
	}
	return left.b == right.b;
}

bool operator==(const LweKeySwitchKey &left, const LweKeySwitchKey &right) {
	if (left.n != right.n) return false;
	if (left.t != right.t) return false;
	if (left.basebit != right.basebit) return false;
	if (left.base != right.base) return false;

	for (int32_t i = 0; i < left.n; ++i) {
		for (int32_t j = 0; j < left.t; ++j) {
			for (int32_t h = 1; h < 1 << left.basebit; ++h) {
				if (!equals(left.ks[i][j][h], right.ks[i][j][h], left.out_params->n)) {
					return false;
				}
			}
		}
	}
	return true;
}

std::ostream &operator<<(std::ostream &os, const LweSample &lweSample) {
	os << "LweSample [" << "current_variance=" << lweSample.current_variance << ", b=" << lweSample.b << ", a={";
	for (unsigned int i{0}; i < 500u; i++) {
		os << lweSample.a[i] << ", ";
	}
	os << "}";
}

std::ostream &operator<<(std::ostream &os, const LweKeySwitchKey &ksk) {
	os << "LweKeySwitchKey [";
	os << "n=" << ksk.n;
	os << ", t=" << ksk.t;
	os << ", basebit=" << ksk.basebit;
	os << ", base=" << ksk.base;
	os << ", ks={" << ksk.base;

	for (int32_t i = 0; i < ksk.n; ++i) {
		os << "{";
		for (int32_t j = 0; j < ksk.t; ++j) {
			os << "{";
			for (int32_t h = 1; h < 1 << ksk.basebit; ++h) {
				os << ksk.ks[i][j][h] << ", ";
			}
			os << "}\n";
		}
		os << "}\n";
	}

	os << "}";
	return os;
}