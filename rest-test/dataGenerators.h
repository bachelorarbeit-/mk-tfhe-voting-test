#ifndef MK_TFHE_VOTING_DATAGENERATORS_H
#define MK_TFHE_VOTING_DATAGENERATORS_H

inline std::vector<std::shared_ptr<MKLweSample>> getMkLweSamples(const Params &params, unsigned int number) {
	std::vector<std::shared_ptr<MKLweSample>> votes{};
	for (unsigned int s{}; s < number; s++) {
		std::shared_ptr<MKLweSample> mkLweSample{std::make_shared<MKLweSample>(params.lweParams.get(), params.mkParams.get())};

		for (unsigned int i = 0; i < mkLweSample->n * mkLweSample->parties; i++) {
			mkLweSample->a[i] = rand();
		}
		votes.push_back(mkLweSample);
	}
	return votes;
}

#endif //MK_TFHE_VOTING_DATAGENERATORS_H
