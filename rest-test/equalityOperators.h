#ifndef MK_TFHE_VOTING_EQUALITYOPERATORS_H
#define MK_TFHE_VOTING_EQUALITYOPERATORS_H

#include <mkTFHEsamples.h>
#include <lwekeyswitch.h>
#include <memory>
#include <ostream>

bool operator==(const MKLweSample &left, const MKLweSample &right);

bool operator==(const std::shared_ptr<MKLweSample> &left, const std::shared_ptr<MKLweSample> &right);

bool operator!=(const std::shared_ptr<MKLweSample> &left, const std::shared_ptr<MKLweSample> &right);

bool equals(const LweSample &left, const LweSample &right, unsigned int n);

bool operator==(const LweKeySwitchKey &left, const LweKeySwitchKey &right);

std::ostream &operator<<(std::ostream &os, const LweSample &lweSample);

std::ostream &operator<<(std::ostream &os, const LweKeySwitchKey &ksk);

#endif //MK_TFHE_VOTING_EQUALITYOPERATORS_H
