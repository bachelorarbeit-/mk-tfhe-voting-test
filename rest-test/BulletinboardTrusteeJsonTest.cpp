#include <gtest/gtest.h>
#include <mk-tfhe-voting/helpers/Params.h>
#include <bulletinboard/rest/JsonConversion.h>
#include <trustee/rest/JsonConversion.h>
#include "dataGenerators.h"
#include "equalityOperators.h"

TEST(BulletinBoardTrustee, SendCounter) {
	Params params{};
	std::vector<std::vector<std::shared_ptr<MKLweSample>>> encryptedCounters{};

	for (unsigned int i{0}; i < 5; ++i) {
		encryptedCounters.push_back(getMkLweSamples(params, 4));
	}

	web::json::value json = jsonFromCounters(0, encryptedCounters);
	auto result{countersFromJson(json, params.lweParams, params.mkParams).second};

	ASSERT_EQ(encryptedCounters, result);
}