#include <gtest/gtest.h>
#include <mk-tfhe-voting/helpers/Params.h>
#include <bulletinboard/rest/JsonConversion.h>
#include <trustee/rest/JsonConversion.h>

struct Counters : testing::Test {
	using counters_t = std::vector<std::vector<std::shared_ptr<MKLweSample>>>;
	counters_t counters{};
	Params params{};


	Counters() {
		for (unsigned i{}; i < params.choices; ++i) {
			std::vector<std::shared_ptr<MKLweSample>> counter{};
			for (unsigned j{}; j < 5; ++j) {
				counter.emplace_back(std::make_shared<MKLweSample>(params.lweParams.get(), params.mkParams.get()));
			}
			counters.push_back(counter);
		}
	}

	bool countersEqual(counters_t const &lhs, counters_t const &rhs) {
		if (lhs.size() != rhs.size()) return false;
		for (unsigned i{}; i < lhs.size(); ++i) {
			if (lhs.at(i).size() != rhs.at(i).size()) return false;
			for (unsigned j{}; j < lhs.at(i).size(); ++j) {
				auto &lhsSample{lhs.at(i).at(j)};
				auto &rhsSample{rhs.at(i).at(j)};
				if (lhsSample->b != rhsSample->b) return false;
				if (lhsSample->current_variance != rhsSample->current_variance) return false;
				if (lhsSample->parties != rhsSample->parties) return false;
				if (lhsSample->n != rhsSample->n) return false;
				for (int k{}; k < lhsSample->n; ++k) {
					if (lhsSample->a[k] != rhsSample->a[k]) return false;
				}
			}
		}
		return true;
	}
};

TEST_F(Counters, Conversion) {
	ASSERT_TRUE(countersEqual(counters, countersFromJson(jsonFromCounters(0u, counters), params.lweParams, params.mkParams).second));
}