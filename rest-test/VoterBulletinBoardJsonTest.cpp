#include <gtest/gtest.h>
#include <mk-tfhe-voting/helpers/Params.h>
#include <bulletinboard/rest/JsonConversion.h>
#include <voter/rest/VoterClient.h>

#include "dataGenerators.h"

TEST(Voter, VoteSerialisation) {
	Params params{};
	std::vector<std::shared_ptr<MKLweSample>> votes{getMkLweSamples(params, 3)};

	auto json{hsr::voteclient::BulletinBoardConnector::getVoteJSON(votes, 0)};

	int n = params.mkParams->parties * params.mkParams->n;
	std::vector<std::shared_ptr<MKLweSample>> ballot{};
	unsigned int ballotId{};
	std::shared_ptr<BulletinBoardParams> bbP{
			std::make_shared<BulletinBoardParams>(0, 1, std::vector<std::weak_ptr<ITrusteeForBulletinBoard>>{}, std::vector<std::shared_ptr<TorusPolynomial>>{}, params)};
	getBallotFromJson(json, bbP, ballot, ballotId);

	ASSERT_EQ(votes.size(), ballot.size());
	for (unsigned int i = 0; i < votes.size(); i++) {
		ASSERT_EQ(votes.at(i)->n, ballot.at(i)->n);
		ASSERT_EQ(votes.at(i)->parties, ballot.at(i)->parties);
		ASSERT_EQ(votes.at(i)->b, ballot.at(i)->b);
		std::vector realA(votes.at(i)->a, votes.at(i)->a + votes.at(i)->parties * votes.at(i)->n);
		std::vector convertedA(ballot.at(i)->a, ballot.at(i)->a + votes.at(i)->parties * votes.at(i)->n);
		ASSERT_EQ(realA, convertedA);
	}
}
