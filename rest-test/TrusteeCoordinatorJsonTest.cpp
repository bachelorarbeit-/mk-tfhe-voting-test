#include <gtest/gtest.h>
#include <mk-tfhe-voting/helpers/Params.h>
#include <bulletinboard/rest/JsonConversion.h>
#include <trustee/rest/JsonConversion.h>

#include "dataGenerators.h"
#include "equalityOperators.h"

TEST(TrusteeCoordinator, SendResults) {
	Params params{};
	std::vector<std::vector<std::shared_ptr<MKLweSample>>> encryptedCounters{};

	for (unsigned int i{0}; i < 5; ++i) {
		encryptedCounters.push_back(getMkLweSamples(params, 4));
	}

	web::json::value json = jsonMapFromCounters(0, encryptedCounters);

	std::vector<std::vector<std::shared_ptr<MKLweSample>>> result{};

	unsigned int index{1};
	while (json.has_field(std::to_string(index))) {
		result.emplace_back(std::vector<std::shared_ptr<MKLweSample>>{});
		for (auto &element : json.at(std::to_string(index)).as_array()) {
			result.back().emplace_back(std::make_shared<MKLweSample>(params.lweParams.get(), params.mkParams.get()));
			for (int j{}; j < params.mkParams.get()->n * params.mkParams.get()->parties; ++j) {
				result.back().back()->a[j] = element.at("phaseShift").at(j).as_number().to_int32();
			}
			result.back().back()->b = element.at("phase").as_number().to_int32();
		}
		index++;
	}

	ASSERT_EQ(encryptedCounters, result);
}