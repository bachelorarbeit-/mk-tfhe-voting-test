#include "../exec/Coordinator.h"
#include <gtest/gtest.h>
#include <mk-tfhe-voting/helpers/Params.h>
#include <bulletinboard/rest/JsonConversion.h>
#include <trustee/rest/JsonConversion.h>
#include "equalityOperators.h"
#include <mkTFHEkeys.h>

struct BootstappingKeys : testing::Test {
	BootstappingKeys() {
		// generate example rLwePubKey
		for (int i{}; i < params.gDimension; ++i) {
			auto torusPolynomial = std::make_shared<TorusPolynomial>(params.N);
			torusPolynomialUniform(torusPolynomial.get());
			rLwePubKey.push_back(torusPolynomial);
		}

		// generate example bootstrappingKey
		for (int i{}; i < params.n; ++i) {
			bootstrappingKey.push_back(std::make_shared<MKTGswUESample>(params.tLweParams.get(), params.mkParams.get()));
		}
	}

	Params params{};
	std::vector<std::shared_ptr<TorusPolynomial>> rLwePubKey{};
	std::vector<std::shared_ptr<MKTGswUESample>> bootstrappingKey{};
	LweKeySwitchKey *ks = new_LweKeySwitchKey_array(1, params.mkParams->n_extract,
													params.mkParams->dks, params.mkParams->Bksbit,
													params.lweParams.get());

	static bool rLwePubKeyEqals(std::vector<std::shared_ptr<TorusPolynomial>> const &lhs, std::vector<std::shared_ptr<TorusPolynomial>> const &rhs) {
		if (lhs.size() != rhs.size()) return false;
		for (unsigned i{}; i < lhs.size(); ++i) {
			if (lhs.at(i)->N != rhs.at(i)->N) return false;
			for (int j{}; j < lhs.at(i)->N; ++j) {
				if (lhs.at(i)->coefsT[j] != rhs.at(i)->coefsT[j]) return false;
			}
		}
		return true;
	}

	static bool bootstrappingKeyEquals(std::vector<std::shared_ptr<MKTGswUESample>> const &lhs, std::vector<std::shared_ptr<MKTGswUESample>> const &rhs) {
		if (lhs.size() != rhs.size()) {
			return false;
		}
		for (unsigned i{}; i < lhs.size(); ++i) {
			if (lhs.at(i)->N != rhs.at(i)->N) {
				return false;
			}
			if (lhs.at(i)->dg != rhs.at(i)->dg) {
				return false;
			}
			/*if (lhs.at(i)->current_variance != rhs.at(i)->current_variance) {
				return false;
			}*/
			if (lhs.at(i)->party != rhs.at(i)->party) {
				return false;
			}
			for (int j{}; j < 6 * lhs.at(i)->dg; ++j) {
				if (!torusPolynomialEquals(rhs.at(i)->c[j], lhs.at(i)->c[j])) {
					return false;
				}
			}
			if (!torusPolynomialEquals(*lhs.at(i)->d, *rhs.at(i)->d)) {
				return false;
			}
			if (!torusPolynomialEquals(*lhs.at(i)->f, *rhs.at(i)->f)) {
				return false;
			}
		}
		return true;
	}

	static bool torusPolynomialEquals(const TorusPolynomial &rhsPoly, const TorusPolynomial &lhsPoly) {
		if (lhsPoly.N != rhsPoly.N) return false;
		for (int k{}; k < lhsPoly.N; ++k) {
			if (lhsPoly.coefsT[k] != rhsPoly.coefsT[k]) return false;
		}
		return true;
	}

	static bool kskEquals(LweKeySwitchKey const *lhs, LweKeySwitchKey const *rhs, unsigned int nbelts) {
		if (lhs->n != rhs->n) return false;
		if (lhs->t != rhs->t) return false;
		if (lhs->basebit != rhs->basebit) return false;
		if (lhs->base != rhs->base) return false;

		for (int32_t i = 0; i < lhs->n; ++i) {
			for (int32_t j = 0; j < lhs->t; ++j) {
				for (int32_t h = 1; h < 1 << lhs->basebit; ++h) {
					if (!equals(lhs->ks[i][j][h], rhs->ks[i][j][h], lhs->out_params->n)) {
						return false;
					}
				}
			}
		}
		return true;
	}
};


TEST_F(BootstappingKeys, kskConversion) {
	LweKeySwitchKey *jsonKs = new_LweKeySwitchKey_array(1, params.mkParams->n_extract,
														params.mkParams->dks, params.mkParams->Bksbit,
														params.lweParams.get());
	fillKsFromJson(jsonKs, jsonFromKsk(ks));
	ASSERT_TRUE(kskEquals(ks, jsonKs, 1));
}

TEST_F(BootstappingKeys, bskConversion) {
	ASSERT_TRUE(bootstrappingKeyEquals(bootstrappingKey, bskFromJson(jsonFromBootstrappingKey(bootstrappingKey), params.tLweParams, params.mkParams)));
}

TEST_F(BootstappingKeys, rlwepubConversion) {
	ASSERT_TRUE(rLwePubKeyEqals(rLwePubKey, rLwePubKeyFromJson(jsonFromRlwePub(rLwePubKey))));
}