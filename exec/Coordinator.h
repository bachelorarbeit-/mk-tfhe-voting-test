#ifndef MK_TFHE_VOTING_COORDINATOR_H
#define MK_TFHE_VOTING_COORDINATOR_H


#include <trustee/Trustee.h>
#include <bulletinboard/BulletinBoard.h>
#include <mk-tfhe-voting/helpers/ICoordinatorForVoter.h>
#include <mk-tfhe-voting/helpers/Params.h>


namespace hsr::coordinator {
	class Coordinator
			: public ICoordinatorForBulletinBoard,
			  public ICoordinatorForTrustee,
			  public ICoordinatorForVoter,
			  public std::enable_shared_from_this<Coordinator> {
	private:
		const Params params;
		std::vector<std::shared_ptr<TorusPolynomial>> rLwePubKey;
		std::shared_ptr<std::vector<std::shared_ptr<LweSample>>> lwePubKeys;
		std::vector<std::shared_ptr<hsr::trustee::Trustee>> trustees;
		std::vector<std::shared_ptr<hsr::bulletinboard::BulletinBoard>> bulletinBoards;
		unsigned trusteeid{0};
		unsigned bulletinboardId{0};
	public:
		explicit Coordinator(const Params &params);

		void setUpTrustee();

		const std::vector<std::shared_ptr<hsr::trustee::Trustee>> &getTrustees() const;

		void setUpBulletinBoards();

		const std::vector<std::shared_ptr<hsr::bulletinboard::BulletinBoard>> &getBulletinBoards();

		void testGates();

		void simulateVotes();

		void registerTrusteeToCoordinator() override;

		void registerBulletinBoardToCoordinator() override;

		std::vector<VotesText> getVotes() override;

		std::map<unsigned int, VotesText> getVotesVoter() override;

		std::shared_ptr<TrusteeParams> registerTrusteeToVote(unsigned voteNumber) override;

		std::shared_ptr<BulletinBoardParams> registerBulletinBoardToVote(unsigned voteNumber) override;

		void publishKeys(std::shared_ptr<std::vector<std::shared_ptr<LweSample>>> pubKey, int32_t n, unsigned voteId) override;

		void signalReady() override;

		void registerVoter() override;

		void trackBallotPaper(unsigned ballotPaperId, std::string const &type) override;

		std::vector<unsigned> checkBallotPaperReceived(unsigned ballotPaperId) override;

		std::shared_ptr<VotingPackage> getVotingPackage(unsigned voteId) override;

		std::vector<unsigned int> getParticipations() override;

		std::map<unsigned int, VoteStatusInformation> getVoteStatusInformation(std::vector<unsigned int> &voteIds) override;

		void trackVoteReceivedPackage(std::vector<unsigned int> const &voteIds) override;

		void trackVoteSentVote(std::vector<unsigned int> const &voteIds) override;

		void publishCounters(unsigned int bulletinBoardId, unsigned int voteId, std::vector<std::vector<std::shared_ptr<MKLweSample>>> vector);

		void publishResults(unsigned int bulletinBoardId, unsigned int voteId, std::vector<std::vector<Torus32>> partialDecrypt) override;
	};
}


#endif //MK_TFHE_VOTING_COORDINATOR_H
