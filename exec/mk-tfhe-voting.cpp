#include <iostream>
#include <iomanip>
#include <ctime>
#include <polynomials.h>

#include <voter/Voter.h>
#include <mk-tfhe-voting/helpers/Debug.h>

#include "Coordinator.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>
#include <mk-tfhe-voting/logging/LoggerFactory.h>

using namespace std;

// **********************************************************************************
// ********************************* MAIN *******************************************
// **********************************************************************************

int32_t main(int32_t argc, char **argv) {
	log4cplus::Logger logger = logger::getDefaultLogger();

	LOG4CPLUS_ERROR(logger, LOG4CPLUS_TEXT("Error"));
	LOG4CPLUS_WARN(logger, LOG4CPLUS_TEXT("Warning"));
	LOG4CPLUS_INFO(logger, LOG4CPLUS_TEXT("Info"));
	LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Debug"));

	Params params{};
	auto coordinator{std::make_shared<hsr::coordinator::Coordinator>(params)};

	cout << "Parties = " << params.trustees << ", B = " << params.gBaseBit << ", gDimension = " << params.gDimension << ", stdevLWE = " << params.stdevLWE << ", stdevRLWEkey = " << params.stdevRLWEkey
		 << ", stdevBK = " << params.stdevBK
		 << endl;

	// Key generation for each trustee
	cout << "Starting KEY GENERATION" << endl;
	clock_t begin_KG = clock();

	coordinator->setUpTrustee();

	// print keyUE
	cout << "keyUE (trustees * n * 6 * gDimension * N * 4 bytes = ";
	cout << params.trustees * params.lweParams->n * 6 * params.gDimension * params.N * 4 / 1024 << " kB)" << endl;
	for (int i = 0; i < params.trustees; i++) {
		for (int j = 0; j < params.lweParams->n; j++) {
			if (j < 2 || j > params.lweParams->n - 3) {
				std::shared_ptr<MKTGswUESample> keyUEij = coordinator->getTrustees().at(i)->getKeyUE().at(j);
				cout << "Party_" << keyUEij->party << ": " << j << endl;
				for (int l = 0; l < 6 * params.gDimension; l++) {
					cout << setw(3) << l;
					printTorusPolynomial(keyUEij->c[l]);
				}
			}
		}
	}

	coordinator->setUpBulletinBoards();

	clock_t end_KG = clock();
	double time_KG = ((double) end_KG - begin_KG) / CLOCKS_PER_SEC;
	cout << "Finished KEY GENERATION " << time_KG << " seconds" << endl;

	coordinator->testGates();

	coordinator->simulateVotes();

	return 0;
}
