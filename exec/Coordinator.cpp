#include "Coordinator.h"
#include <mk-tfhe-voting/helpers/Encryption.h>
#include <voter/Voter.h>
#include <mk-tfhe-voting/helpers/VotingPackage.h>
#include <bulletinboard/BulletinBoard.h>
#include <mk-tfhe-voting/helpers/IBulletinBoardForVoter.h>
#include <mk-tfhe-voting/helpers/IBulletinBoardForTrustee.h>

#include <iostream>
#include <iomanip>
#include <memory>
#include <chrono>

namespace hsr::coordinator {

	Coordinator::Coordinator(const Params &params) : params{params}, rLwePubKey{}, lwePubKeys{std::make_shared<std::vector<std::shared_ptr<LweSample>>>()}, trustees{}, bulletinBoards{} {
		for (unsigned int i{0}; i < static_cast<unsigned int>(params.gDimension); i++) {
			rLwePubKey.push_back(std::make_shared<TorusPolynomial>(params.N));
		}
		for (int j = 0; j < params.gDimension; j++) {
			torusPolynomialUniform(rLwePubKey.at(j).get());
		}
	}

	void Coordinator::setUpTrustee() {
		for (unsigned int p = 0; p < params.trustees; p++) {
			std::shared_ptr<ICoordinatorForTrustee> pointer{shared_from_this()};
			trustees.push_back(std::make_shared<hsr::trustee::Trustee>(pointer));
		}
	}

	const std::vector<std::shared_ptr<hsr::trustee::Trustee>> &Coordinator::getTrustees() const {
		return trustees;
	}

	void Coordinator::setUpBulletinBoards() {
		for (unsigned int i{0}; i < params.bulletinBoards; i++) {
			std::shared_ptr<ICoordinatorForBulletinBoard> pointer{shared_from_this()};
			auto teller{std::make_shared<hsr::bulletinboard::BulletinBoard>(pointer, i)};
			teller->generateKeys();
			this->bulletinBoards.push_back(teller);
		}
	}

	const std::vector<std::shared_ptr<hsr::bulletinboard::BulletinBoard>> &Coordinator::getBulletinBoards() {
		return this->bulletinBoards;
	}

	void Coordinator::testGates() {
		const int32_t nb_trials = 10;

		int32_t error_count = 0;
		double argv_time_NAND = 0.0;

		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<int32_t> dis(0, 1);

		for (unsigned int tId{0}; tId < this->bulletinBoards.size(); tId++) {
			std::cout << "********************************" << std::endl;
			std::cout << "BulletinBoard: " << tId << std::endl;
			std::cout << "********************************" << std::endl;
			for (int trial = 0; trial < nb_trials; ++trial) {
				std::cout << "****************" << std::endl;
				std::cout << "Trial: " << trial << std::endl;

				int32_t b1{dis(gen)};
				int32_t b2{dis(gen)};
				int32_t b_NAND = !(b1 && b2);
				int32_t b_NOR = !(b1 || b2);
				int32_t b_ANDYN = b1 && !b2;
				int32_t b_ANDNY = !b1 && b2;
				int32_t b_AND = b1 && b2;
				int32_t b_XOR = b1 != b2;

				// generate 2 samples in input
				MKLweSample s1{params.lweParams.get(), params.mkParams.get()};
				MKLweSample s2{params.lweParams.get(), params.mkParams.get()};
				hsr::helpers::mySymEncrypt(&s1, b1, params.generatedKeys, params.encryptionKeys, *lwePubKeys);
				hsr::helpers::mySymEncrypt(&s2, b2, params.generatedKeys, params.encryptionKeys, *lwePubKeys);

				// generate NAND, NOR and ANDNY output sample
				MKLweSample s_NAND{params.lweParams.get(), params.mkParams.get()};
				MKLweSample s_NOR{params.lweParams.get(), params.mkParams.get()};
				MKLweSample s_ANDYN{params.lweParams.get(), params.mkParams.get()};
				MKLweSample s_ANDNY{params.lweParams.get(), params.mkParams.get()};
				MKLweSample s_AND{params.lweParams.get(), params.mkParams.get()};
				MKLweSample s_XOR{params.lweParams.get(), params.mkParams.get()};

				const std::vector<std::shared_ptr<hsr::trustee::ITrustee>> trusteesList{this->trustees.begin(), this->trustees.end()};

				// verify encrypt
				std::cout << "Bit 1: clear = " << b1 << ", decrypted = ";
				std::cout << hsr::helpers::mySymDecrypt(&s1, trusteesList) << " (b =";
				std::cout << std::setw(12) << s1.b << ", alpha = " << std::setprecision(5);
				std::cout << sqrt(s1.current_variance) << ")" << std::endl;
				std::cout << "Bit 2: clear = " << b2 << ", decrypted = ";
				std::cout << hsr::helpers::mySymDecrypt(&s2, trusteesList) << " (b =";
				std::cout << std::setw(12) << s2.b << ", alpha = " << std::setprecision(5);
				std::cout << sqrt(s2.current_variance) << ")" << std::endl;

				// evaluate MK bootstrapped NAND
				clock_t begin = clock();
				this->bulletinBoards.at(tId)->bsNAND(s_NAND, s1, s2);
				clock_t end = clock();
				double time = ((double) end - begin) / CLOCKS_PER_SEC;
				argv_time_NAND += time;
				hsr::helpers::verifyGate("NAND: ", &s_NAND, b_NAND, trusteesList, &error_count, trial);

				// evaluate MK bootstrapped NOR
				this->bulletinBoards.at(tId)->bsNOR(s_NOR, s1, s2);
				hsr::helpers::verifyGate("NOR:  ", &s_NOR, b_NOR, trusteesList, &error_count, trial);

				// evaluate MK bootstrapped ANDYN
				this->bulletinBoards.at(tId)->bsANDYN(s_ANDYN, s1, s2);
				hsr::helpers::verifyGate("ANDYN:", &s_ANDYN, b_ANDYN, trusteesList, &error_count, trial);

				// evaluate MK bootstrapped ANDNY
				this->bulletinBoards.at(tId)->bsANDNY(s_ANDNY, s1, s2);
				hsr::helpers::verifyGate("ANDNY:", &s_ANDNY, b_ANDNY, trusteesList, &error_count, trial);

				// evaluate MK bootstrapped AND
				this->bulletinBoards.at(tId)->bsAND(s_AND, s1, s2);
				hsr::helpers::verifyGate("AND:  ", &s_AND, b_AND, trusteesList, &error_count, trial);

				// evaluate MK bootstrapped XOR
				this->bulletinBoards.at(tId)->bsXOR(s_XOR, s1, s2);
				hsr::helpers::verifyGate("XOR:  ", &s_XOR, b_XOR, trusteesList, &error_count, trial);

				std::cout << "Time per MKbootNAND gate: " << time << " seconds" << std::endl;

			}
		}
		std::cout << std::endl;
		std::cout << "ERRORS: " << error_count << " over " << 6 * nb_trials * this->bulletinBoards.size() << " tests!" << std::endl;
		std::cout << "Average time per MKbootNAND: " << argv_time_NAND / (nb_trials * static_cast<double>(this->bulletinBoards.size())) << " seconds" << std::endl;
	}

	void Coordinator::simulateVotes() {
		const int32_t nb_trials = 10;
		for (int trial = 0; trial < nb_trials; ++trial) {
			std::shared_ptr<ICoordinatorForVoter> coord = shared_from_this();
			std::cout << "Starting voter: " << trial << std::endl;
			voter::Voter voter{coord};
			std::map m{std::make_pair(1u, getVotingPackage(1))};
			voter.setVotingPackage(m);
			voter.vote();
		}
	}

	void Coordinator::registerTrusteeToCoordinator() {

	}

	void Coordinator::registerBulletinBoardToCoordinator() {

	}

	std::vector<VotesText> Coordinator::getVotes() {
		return std::vector<VotesText>{};
	}

	std::map<unsigned int, VotesText> Coordinator::getVotesVoter() {
		return std::map<unsigned int, VotesText>{};
	}

	std::shared_ptr<TrusteeParams> Coordinator::registerTrusteeToVote(unsigned) {
		return std::make_shared<TrusteeParams>(trusteeid++, 0, std::vector<std::shared_ptr<IBulletinBoardForTrustee>>(bulletinBoards.begin(), bulletinBoards.end()), rLwePubKey, params);
	}

	std::shared_ptr<BulletinBoardParams> Coordinator::registerBulletinBoardToVote(unsigned) {
		return std::make_shared<BulletinBoardParams>(bulletinboardId++, 0, std::vector<std::weak_ptr<ITrusteeForBulletinBoard>>(trustees.begin(), trustees.end()), rLwePubKey, params);
	}

	void Coordinator::publishKeys(std::shared_ptr<std::vector<std::shared_ptr<LweSample>>> publicKeys, int32_t, unsigned voteId) {
		lwePubKeys->insert(lwePubKeys->end(), publicKeys->begin(), publicKeys->end());
	}

	void Coordinator::signalReady() {

	}

	void Coordinator::registerVoter() {
	}

	void Coordinator::trackBallotPaper(unsigned, std::string const &) {

	}

	std::vector<unsigned> Coordinator::checkBallotPaperReceived(unsigned) {
		std::vector<unsigned> readyBulletinBoards{};
		for (unsigned i{}; i < bulletinBoards.size(); ++i) {
			readyBulletinBoards.push_back(i);
		}
		return readyBulletinBoards;
	}

	std::shared_ptr<VotingPackage> Coordinator::getVotingPackage(unsigned voteId) {
		return std::make_shared<VotingPackage>(voteId, std::vector<std::weak_ptr<IBulletinBoardForVoter>>(bulletinBoards.begin(), bulletinBoards.end()),
											   lwePubKeys, params, VotesText{}, std::tm{}, std::tm{});
	}

	std::vector<unsigned int> Coordinator::getParticipations() {
		return std::vector<unsigned int>();
	}

	std::map<unsigned int, VoteStatusInformation> Coordinator::getVoteStatusInformation(std::vector<unsigned int> &) {
		return std::map<unsigned int, VoteStatusInformation>();
	}

	void Coordinator::trackVoteReceivedPackage(std::vector<unsigned int> const &) {

	}

	void Coordinator::trackVoteSentVote(std::vector<unsigned int> const &) {

	}

	void Coordinator::publishCounters(unsigned int bulletinBoardId, unsigned int voteId, std::vector<std::vector<std::shared_ptr<MKLweSample>>> vector) {

	}

	void Coordinator::publishResults(unsigned int bulletinBoardId, unsigned int voteId, std::vector<std::vector<Torus32>> partialDecrypt) {

	}
}