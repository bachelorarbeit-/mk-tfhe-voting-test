#include "GateTest.h"

namespace test::gates {
	void GateTest::SetUp() {
		this->params = std::make_shared<Params>();
		this->coordinator = std::make_shared<hsr::coordinator::Coordinator>(*params);
		this->coordinator->setUpTrustee();
		this->coordinator->setUpBulletinBoards();
	}

	std::shared_ptr<Params> GateTest::getParams() {
		return this->params;
	}

	std::vector<std::shared_ptr<hsr::trustee::Trustee>> GateTest::getTrustees() {
		return this->coordinator->getTrustees();
	}

	std::shared_ptr<hsr::bulletinboard::BulletinBoard> GateTest::getTeller() {
		return this->coordinator->getBulletinBoards().at(0);
	}

	void GateTest::testGate(int32_t (*model_gate)(int32_t const, int32_t const), void (hsr::bulletinboard::BulletinBoard::*boots_gate)(MKLweSample &, MKLweSample const &, MKLweSample const &) const) {
		const int32_t nb_trials = 10;
		int32_t error_count = 0;

		std::vector<std::shared_ptr<LweSample>> lwePubKey{};
		for (std::size_t p = 0; p < getParams()->trustees; p++) {
			std::shared_ptr<std::vector<std::shared_ptr<LweSample>>> publicKeys{getTrustees().at(p)->getPublicKeys()};
			lwePubKey.insert(lwePubKey.end(), publicKeys->begin(), publicKeys->end());
		}

		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<int32_t> dis(0, 1);

		std::vector<std::shared_ptr<hsr::trustee::ITrustee>> trustees{};
		for (auto trustee : coordinator->getTrustees()) {
			trustees.push_back(trustee);
		}

		for (int trial = 0; trial < nb_trials; ++trial) {
			int32_t b1{dis(gen)};
			int32_t b2{dis(gen)};
			int32_t b_XOR = model_gate(b1, b2);

			// generate 2 samples in input
			MKLweSample s1{getParams()->lweParams.get(), getParams()->mkParams.get()};
			MKLweSample s2{getParams()->lweParams.get(), getParams()->mkParams.get()};
			hsr::helpers::mySymEncrypt(&s1, b1, getParams()->generatedKeys, getParams()->encryptionKeys, lwePubKey);
			hsr::helpers::mySymEncrypt(&s2, b2, getParams()->generatedKeys, getParams()->encryptionKeys, lwePubKey);

			MKLweSample s_XOR{getParams()->lweParams.get(), getParams()->mkParams.get()};

			// evaluate MK bootstrapped XOR
			((*getTeller()).*boots_gate)(s_XOR, s1, s2);
			hsr::helpers::verifyGate("XOR:  ", &s_XOR, b_XOR, trustees, &error_count, trial, false);
		}

		EXPECT_EQ(error_count, 0);
	}

	int32_t nandGate(int32_t a, int32_t b) {
		return !(a && b);
	}

	int32_t norGate(int32_t a, int32_t b) {
		return !(a || b);
	}

	int32_t andynGate(int32_t a, int32_t b) {
		return a && !b;
	}

	int32_t andnyGate(int32_t a, int32_t b) {
		return !a && b;
	}

	int32_t andGate(int32_t a, int32_t b) {
		return a && b;
	}

	int32_t xorGate(int32_t a, int32_t b) {
		return a != b;
	}

	int32_t orGate(int32_t a, int32_t b) {
		return a || b;
	}

	TEST_F(GateTest, NAND) {
		testGate(nandGate, &hsr::bulletinboard::BulletinBoard::bsNAND);
	}

	TEST_F(GateTest, NOR) {
		testGate(norGate, &hsr::bulletinboard::BulletinBoard::bsNOR);
	}

	TEST_F(GateTest, ANDYN) {
		testGate(andynGate, &hsr::bulletinboard::BulletinBoard::bsANDYN);
	}

	TEST_F(GateTest, ANDNY) {
		testGate(andnyGate, &hsr::bulletinboard::BulletinBoard::bsANDNY);
	}

	TEST_F(GateTest, AND) {
		testGate(andGate, &hsr::bulletinboard::BulletinBoard::bsAND);
	}

	TEST_F(GateTest, XOR) {
		testGate(xorGate, &hsr::bulletinboard::BulletinBoard::bsXOR);
	}

	TEST_F(GateTest, OR) {
		testGate(orGate, &hsr::bulletinboard::BulletinBoard::bsOR);
	}
}