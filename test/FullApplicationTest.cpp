#include <voter/Voter.h>
#include <mk-tfhe-voting/helpers/Encryption.h>
#include <gtest/gtest.h>
#include <tuple>

#include "../exec/Coordinator.h"


class FullApplicationTest : public ::testing::TestWithParam<unsigned> {
protected:
	virtual void SetUp() {

	}

	void initialial() {
		this->params = std::make_shared<Params>(1, 500, 0.0078125, 2, 8, 2.44e-5, 1024, 3.29e-10, 3.29e-10, 3.29e-10,
												4, 8, 3.29e-10, 3, 8192, 12, 1, 5);
		this->coordinator = std::make_shared<hsr::coordinator::Coordinator>(*params);

		coordinator->setUpTrustee();
		coordinator->setUpBulletinBoards();
	}

	std::vector<unsigned int> decryptCounter() {
		std::vector<std::shared_ptr<hsr::trustee::ITrustee>> trustees{};
		for (const auto &trustee : coordinator->getTrustees()) {
			trustees.push_back(trustee);
		}
		std::vector<unsigned int> results{};
		for (auto &teller : coordinator->getBulletinBoards()) {
			for (auto &counter : teller->getCounters()) {
				unsigned int result{0};
				unsigned int offset{0};
				for (auto &bit : counter) {
					result += hsr::helpers::mySymDecrypt(bit.get(), trustees) << offset++;
				}
				results.push_back(result);
			}
		}
		return results;
	}

	std::shared_ptr<Params> params;
	std::shared_ptr<hsr::coordinator::Coordinator> coordinator;
	std::unique_ptr<hsr::voter::Voter> voter;
};


TEST_P(FullApplicationTest, MultipleBulletinBoardsMultipleVoter) {
	initialial();
	unsigned int numberOfVoters{GetParam()};
	std::vector<unsigned int> results(this->params->choices, 0);

	std::vector<unsigned int> elections{};
	elections.emplace_back(1);

	for (unsigned int voterId{0}; voterId < numberOfVoters; voterId++) {
		unsigned int choice = rand() % this->params->choices;
		voter = std::make_unique<hsr::voter::Voter>(std::weak_ptr<ICoordinatorForVoter>{this->coordinator});
		std::map m{std::make_pair(1u, this->coordinator->getVotingPackage(voterId))};
		voter->setVotingPackage(m);
		results.at(choice)++;

		std::vector<unsigned int> choices{};
		choices.emplace_back(choice);
		voter->vote(elections, choices);
	}

	std::this_thread::sleep_for(std::chrono::seconds(1));

	for (const auto &bulletinBoard : this->coordinator->getBulletinBoards()) {
		bulletinBoard->voteDone();
	}

	std::vector<unsigned int> lweResults{decryptCounter()};
	std::cout << "votes: ";
	std::copy(results.begin(), results.end(), std::ostream_iterator<unsigned int>(std::cout, ","));
	ASSERT_EQ(results, lweResults);
}

INSTANTIATE_TEST_SUITE_P (
		MultipleTest,
		FullApplicationTest,
		::testing::Values(
				1u,
				2u,
				4u,
				8u,
				16u,
				32u,
				64u
		)
);
