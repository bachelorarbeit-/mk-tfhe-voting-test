#include <gtest/gtest.h>
#include <memory>
#include <random>

#include <mk-tfhe-voting/data/DataRepository.h>
#include "DataTest.h"

using ::testing::EmptyTestEventListener;
using ::testing::InitGoogleTest;
using ::testing::Test;
using ::testing::TestEventListeners;
using ::testing::TestInfo;
using ::testing::TestPartResult;
using ::testing::UnitTest;

namespace test::data {
	std::ostream &operator<<(std::ostream &os, hsr::data::model::LwePublicKey lwePublicKey) {
		os << "LwePublicKey{id=" << lwePublicKey.getId() << ", length=" << lwePublicKey.getLength() << ", phaseShift=[";
		for (int phaseShift : lwePublicKey.getPhaseShift()) {
			os << phaseShift << ", ";
		}
		os << "], phase=" << lwePublicKey.getPhase() << "}";
		return os;
	}

	std::vector<int> generatePhaseShifts(unsigned int length) {
		std::random_device r;
		std::default_random_engine generator(r());
		std::uniform_int_distribution<int> distribution(std::numeric_limits<int>::min(), std::numeric_limits<int>::max());
		std::vector<int> ps{};
		for (unsigned int i{}; i < length; i++) {
			ps.push_back(distribution(generator));
		}
		return ps;
	}

	TEST(DataRepository, PublicKeysStoreAndLoad) {
		std::vector<int> ps{generatePhaseShifts(20)};
		hsr::data::model::LwePublicKey key{0, static_cast<unsigned int>(ps.size()), ps, 5};
		hsr::data::DataRepository repo{};
		key = repo.store(key);
		std::vector<hsr::data::model::LwePublicKey> samples{repo.findAllLwePublicKeys()};
		ASSERT_EQ(key, samples.at(samples.size() - 1));
	}

	TEST(DataRepository, PublicKeysStoreAndLoadLists) {
		std::vector<hsr::data::model::LwePublicKey> lwePublicKeys{};
		for (int i = 0; i < 10; i++) {
			std::vector<int> ps{generatePhaseShifts(20)};
			lwePublicKeys.push_back(hsr::data::model::LwePublicKey{0, static_cast<unsigned int>(ps.size()), ps, 5});
		}
		hsr::data::DataRepository repo{};
		repo.storeAll(lwePublicKeys);
		std::vector<hsr::data::model::LwePublicKey> allSamples{repo.findAllLwePublicKeys()};
		std::vector<hsr::data::model::LwePublicKey> newSamples{(allSamples.end() - lwePublicKeys.size()), allSamples.end()};
		ASSERT_EQ(lwePublicKeys, newSamples);
	}

	std::vector<std::byte> generateKeyBytes(unsigned int length) {
		std::random_device r;
		std::default_random_engine generator(r());
		std::uniform_int_distribution<int> distribution(0, 1);
		std::vector<std::byte> keyBits{};
		for (unsigned int i = 0; i < (static_cast<unsigned int>(length) / 8u + 1u); i++) {
			std::byte keyBit{0};
			for (unsigned int b{}; b < 8; b++) {
				unsigned int keyIndex{8 * i + b};
				if (keyIndex < length) {
					keyBit |= static_cast<std::byte>(distribution(generator) << b);
				}
			}
			keyBits.push_back(keyBit);
		}
		return keyBits;
	}

	TEST(DataRepository, PrivateKeysStoreAndLoad) {
		std::vector<std::byte> keyBits{generateKeyBytes(20u)};
		hsr::data::model::LwePrivateKey key{0, static_cast<unsigned int>(keyBits.size()), keyBits};
		hsr::data::DataRepository repo{};
		key = repo.store(key);
		std::vector<hsr::data::model::LwePrivateKey> samples{repo.findAllLwePrivateKeys()};
		ASSERT_EQ(key, samples.at(samples.size() - 1));
	}

	TEST(DataRepository, PrivateKeysStoreAndLoadLists) {
		std::vector<hsr::data::model::LwePrivateKey> lwePrivateKeys{};
		for (int i = 0; i < 10; i++) {
			std::vector<std::byte> keyBits{generateKeyBytes(20u)};
			lwePrivateKeys.emplace_back(0, static_cast<unsigned int>(keyBits.size()), keyBits);
		}
		hsr::data::DataRepository repo{};
		repo.storeAll(lwePrivateKeys);
		std::vector<hsr::data::model::LwePrivateKey> allSamples{repo.findAllLwePrivateKeys()};
		std::vector<hsr::data::model::LwePrivateKey> newSamples{(allSamples.end() - lwePrivateKeys.size()), allSamples.end()};
		ASSERT_EQ(lwePrivateKeys, newSamples);
	}
}