#ifndef MK_TFHE_VOTING_GATETEST_H
#define MK_TFHE_VOTING_GATETEST_H

#include <gtest/gtest.h>
#include <memory>

#include <mk-tfhe-voting/helpers/Encryption.h>
#include "../exec/Coordinator.h"

using ::testing::EmptyTestEventListener;
using ::testing::InitGoogleTest;
using ::testing::Test;
using ::testing::TestEventListeners;
using ::testing::TestInfo;
using ::testing::TestPartResult;
using ::testing::UnitTest;


namespace test::gates {
	class GateTest : public ::testing::Test {
	private:
		std::shared_ptr<Params> params;
		std::shared_ptr<hsr::coordinator::Coordinator> coordinator;
	public:
		void SetUp();

		std::shared_ptr<Params> getParams();

		std::vector<std::shared_ptr<hsr::trustee::Trustee>> getTrustees();

		std::shared_ptr<hsr::bulletinboard::BulletinBoard> getTeller();

		void testGate(int32_t (*model_gate)(int32_t const, int32_t const), void (hsr::bulletinboard::BulletinBoard::*boots_gate)(MKLweSample &, MKLweSample const &, MKLweSample const &) const);
	};
}

#endif //MK_TFHE_VOTING_GATETEST_H
