#include <voter/Voter.h>
#include <mk-tfhe-voting/helpers/Encryption.h>
#include <gtest/gtest.h>
#include <tuple>

#include "../exec/Coordinator.h"


class IntegrationTest : public ::testing::TestWithParam<std::tuple<unsigned, unsigned>> {
protected:
	virtual void SetUp() {

	}

	void initialial(unsigned numberOfTrustees, unsigned numberOfTellers) {
		this->params = std::make_shared<Params>(1, 500, 0.0078125, 2, 8, 2.44e-5, 1024, 3.29e-10, 3.29e-10, 3.29e-10,
												4, 8, 3.29e-10, numberOfTrustees, 8192, 2, numberOfTellers, 2);
		this->coordinator = std::make_shared<hsr::coordinator::Coordinator>(*params);

		coordinator->setUpTrustee();
		coordinator->setUpBulletinBoards();
	}

	std::vector<std::vector<std::vector<bool>>> decryptCounter() {
		std::vector<std::shared_ptr<hsr::trustee::ITrustee>> trustees{};
		for (auto trustee : coordinator->getTrustees()) {
			trustees.push_back(trustee);
		}

		std::vector<std::vector<std::vector<bool>>> decryptedTeller;
		for (auto &teller : coordinator->getBulletinBoards()) {
			decryptedTeller.emplace_back();
			auto &decryptedCounter = decryptedTeller.back();
			for (auto &counter : teller->getCounters()) {
				decryptedCounter.emplace_back();
				for (auto &bit : counter) {
					decryptedCounter.back().emplace_back(hsr::helpers::mySymDecrypt(bit.get(), trustees));
				}
			}
		}
		return decryptedTeller;
	}

	void printCounter() {
		std::vector<std::vector<std::vector<bool>>> counters{decryptCounter()};
		for (std::size_t i{}; i < counters.size(); ++i) {
			ASSERT_EQ(counters[0], counters[i]);
		}
	}

	std::shared_ptr<Params> params;
	std::shared_ptr<hsr::coordinator::Coordinator> coordinator;
	std::unique_ptr<hsr::voter::Voter> voter;
};


TEST_P(IntegrationTest, MultipleBulletinBoardsMultipleVoter) {
	initialial(std::get<1>(GetParam()), std::get<0>(GetParam()));

	voter = std::make_unique<hsr::voter::Voter>(std::weak_ptr<ICoordinatorForVoter>{this->coordinator});
	std::map m{std::make_pair(0u, this->coordinator->getVotingPackage(0))};
	voter->setVotingPackage(m);
	voter->vote();

	std::this_thread::sleep_for(std::chrono::seconds(1));
	for (const auto &bulletinBoard : this->coordinator->getBulletinBoards()) {
		bulletinBoard->voteDone();
	}

	printCounter();
}

INSTANTIATE_TEST_SUITE_P (
		MultipleTest,
		IntegrationTest,
		::testing::Values(
				std::make_tuple(1u, 1u),
				std::make_tuple(2u, 1u),
				std::make_tuple(3u, 1u),
				std::make_tuple(4u, 1u),
				std::make_tuple(1u, 2u),
				std::make_tuple(2u, 2u),
				std::make_tuple(3u, 2u),
				std::make_tuple(4u, 2u),
				std::make_tuple(1u, 3u),
				std::make_tuple(2u, 3u),
				std::make_tuple(3u, 3u),
				std::make_tuple(4u, 3u),
				std::make_tuple(1u, 4u),
				std::make_tuple(2u, 4u),
				std::make_tuple(3u, 4u),
				std::make_tuple(4u, 4u)
		)
);
