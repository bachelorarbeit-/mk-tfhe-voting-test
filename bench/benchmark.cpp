#include "benchmark/benchmark.h"

#include <mk-tfhe-voting/helpers/Params.h>
#include <mk-tfhe-voting/helpers/Encryption.h>
#include "../exec/Coordinator.h"
#include <bulletinboard/BulletinBoard.h>

#include <memory>
#include <string>

Params params{};
std::shared_ptr<hsr::coordinator::Coordinator> coordinator{std::make_shared<hsr::coordinator::Coordinator>(params)};
std::vector<std::shared_ptr<LweSample>> lwePubKey{};

std::random_device rd;
std::mt19937 gen(rd());
std::uniform_int_distribution<int32_t> dis(0, 1);

bool isSetUp = false;

void setUp() {
	if (!isSetUp) {
		coordinator->setUpTrustee();
		coordinator->setUpBulletinBoards();

		for (int p = 0; p < params.trustees; p++) {
			std::shared_ptr<std::vector<std::shared_ptr<LweSample>>> publicKeys{coordinator->getTrustees().at(p)->getPublicKeys()};
			lwePubKey.insert(lwePubKey.end(), publicKeys->begin(), publicKeys->end());
		}

		isSetUp = true;
	}
}

bool nandGate(bool a, bool b) { return !(a && b); }

bool norGate(bool a, bool b) { return !(a || b); }

bool andynGate(bool a, bool b) { return a && !b; }

bool andnyGate(bool a, bool b) { return !a && b; }

bool andGate(bool a, bool b) { return a && b; }

bool xorGate(bool a, bool b) { return a != b; }

bool orGate(bool a, bool b) { return a || b; }

void benchmarkGate(int &error, bool (*model_gate)(bool, bool), void (hsr::bulletinboard::BulletinBoard::*boots_gate)(MKLweSample &, MKLweSample const &, MKLweSample const &) const) {
	int32_t b1{dis(gen)};
	int32_t b2{dis(gen)};
	MKLweSample s1{params.lweParams.get(), params.mkParams.get()};
	MKLweSample s2{params.lweParams.get(), params.mkParams.get()};
	hsr::helpers::mySymEncrypt(&s1, b1, params.generatedKeys, params.encryptionKeys, lwePubKey);
	hsr::helpers::mySymEncrypt(&s2, b2, params.generatedKeys, params.encryptionKeys, lwePubKey);

	MKLweSample s_XOR{params.lweParams.get(), params.mkParams.get()};
	((*coordinator->getBulletinBoards().at(0)).*boots_gate)(s_XOR, s1, s2);
	int32_t b_XOR = model_gate(b1, b2);

	std::vector<std::shared_ptr<hsr::trustee::ITrustee>> trustees{};
	for (auto trustee : coordinator->getTrustees()) {
		trustees.push_back(trustee);
	}
	hsr::helpers::verifyGate("XOR:  ", &s_XOR, b_XOR, trustees, &error, 0, false);
}

static void BM_nandGate(benchmark::State &state) {
	if (state.thread_index == 0) {
		setUp();
	}
	int counter{0};
	while (state.KeepRunning()) {
		int error{};
		benchmarkGate(error, &nandGate, &hsr::bulletinboard::BulletinBoard::bsNAND);
		if (error > 0) {
			state.SkipWithError((std::string{"fehler aufgetreten nach "} + std::to_string(counter)).c_str());
		}
		++counter;
	}
}

BENCHMARK(BM_nandGate)->Threads(8)->Iterations(10);

static void BM_norGate(benchmark::State &state) {
	if (state.thread_index == 0) {
		setUp();
	}
	int counter{0};
	while (state.KeepRunning()) {
		int error{};
		benchmarkGate(error, &norGate, &hsr::bulletinboard::BulletinBoard::bsNOR);
		if (error > 0) {
			state.SkipWithError((std::string{"fehler aufgetreten nach "} + std::to_string(counter)).c_str());
		}
		++counter;
	}
}

BENCHMARK(BM_norGate)->Threads(8)->Iterations(10);

static void BM_andGate(benchmark::State &state) {
	if (state.thread_index == 0) {
		setUp();
	}
	int counter{0};
	while (state.KeepRunning()) {
		int error{};
		benchmarkGate(error, &andGate, &hsr::bulletinboard::BulletinBoard::bsAND);
		if (error > 0) {
			state.SkipWithError((std::string{"fehler aufgetreten nach "} + std::to_string(counter)).c_str());
		}
		++counter;
	}
}

BENCHMARK(BM_andGate)->Threads(8)->Iterations(10);

static void BM_andynGate(benchmark::State &state) {
	if (state.thread_index == 0) {
		setUp();
	}
	int counter{0};
	while (state.KeepRunning()) {
		int error{};
		benchmarkGate(error, &andynGate, &hsr::bulletinboard::BulletinBoard::bsANDYN);
		if (error > 0) {
			state.SkipWithError((std::string{"fehler aufgetreten nach "} + std::to_string(counter)).c_str());
		}
		++counter;
	}
}

BENCHMARK(BM_andynGate)->Threads(8)->Iterations(10);

static void BM_andnyGate(benchmark::State &state) {
	if (state.thread_index == 0) {
		setUp();
	}
	int counter{0};
	while (state.KeepRunning()) {
		int error{};
		benchmarkGate(error, &andnyGate, &hsr::bulletinboard::BulletinBoard::bsANDNY);
		if (error > 0) {
			state.SkipWithError((std::string{"fehler aufgetreten nach "} + std::to_string(counter)).c_str());
		}
		++counter;
	}
}

BENCHMARK(BM_andnyGate)->Threads(8)->Iterations(10);

static void BM_xorGate(benchmark::State &state) {
	if (state.thread_index == 0) {
		setUp();
	}
	int counter{0};
	while (state.KeepRunning()) {
		int error{};
		benchmarkGate(error, &xorGate, &hsr::bulletinboard::BulletinBoard::bsXOR);
		if (error > 0) {
			state.SkipWithError((std::string{"fehler aufgetreten nach "} + std::to_string(counter)).c_str());
		}
		++counter;
	}
}

BENCHMARK(BM_xorGate)->Threads(8)->Iterations(10);

static void BM_orGate(benchmark::State &state) {
	if (state.thread_index == 0) {
		setUp();
	}
	int counter{0};
	while (state.KeepRunning()) {
		int error{};
		benchmarkGate(error, &orGate, &hsr::bulletinboard::BulletinBoard::bsOR);
		if (error > 0) {
			state.SkipWithError((std::string{"fehler aufgetreten nach "} + std::to_string(counter)).c_str());
		}
		++counter;
	}
}

BENCHMARK(BM_orGate)->Threads(8)->Iterations(10);


BENCHMARK_MAIN();